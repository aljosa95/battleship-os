/**
 * @file client.c
 * @author Alija-Ali Abadzic 01427575 <e01427575@student.tuwien.ac.at>
 * @date Noevember 2017
 *
 * @brief Client for OSUE exercise 1B `Battleship'.
 * In this server file is client socket implementation and also
 * some implementation parts of game battleship. The rest is in server.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>

// stuff shared by client and server:
#include "common.h"

#include <arpa/inet.h>

#define PORT "1280" // the port client will be connecting to

// Static variables for resources that should be freed before exiting:
static struct addrinfo *ai = NULL; // addrinfo struct
static int sockfd = -1;			   // socket file descriptor

// Static variables for things you might want to access from several functions:
static const char *port = DEFAULT_PORT; // the port to bind to
static char *prog_name = "client";
static char *host = "localhost";

/**
 * Mandatory usage function.
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: prog_name
 **/
static void usage(void){
	(void)fprintf(stderr, "Usage: %s [-h HOSTNAME] [-p PORT]\n", prog_name);
	exit(EXIT_FAILURE);
}

/**
 * @brief Function that calculate one random try for every round in range of 0-99
 * @return random_num Random number in range 0-99
 */
static uint8_t random_try(void){
	int random_num = rand() % 100;
	return random_num;
}

/**
 * @brief Function that hang a parity bit to the message that
 * client sends to server and this represents coordinate in a field.
 * @param send_data 8 bit integer value that contains coordinate info without parity
 * @return message with parity at the begin of 8 bit value
 */
static uint8_t hang_parity(uint8_t send_data){
	int k = 1;
	uint8_t count_ones = 0;
	for (int i = 0; i < 8; i++)
	{
		if (send_data & (k << i))
		{
			count_ones++;
		}
	}
	if (count_ones % 2 == 0)
	{
		send_data = send_data & 0x7F;
	}
	else
	{
		send_data = send_data | 0x80;
	}
	return send_data;
}

/**
 * @brief Function that parses all arguments that we write as we start the programme.
 * @details global variables: prog_name, port
 * @param argc The argument counter.
 * @param argv The argument vector
 */
static void parse_args(int argc, char **argv){
	int c;
	if (argc < 0 || argc > 5)
	{
		usage();
	}

	while ((c = getopt(argc, argv, "h:p:")) != -1)
	{
		switch (c)
		{
		case 'p':
			port = optarg;
			break;
		case 'h':
			host = optarg;
			break;
		case '?':
			usage();
			break;
		default:
			usage();
			break;
		}
	}

	if (argc != 5 && argc != 1 && argc != 3)
	{
		usage();
	}

	(void)fprintf(stdout, "./%s PORT: %s \n",prog_name, port);
	fflush(stdout);
}


/**
 * @brief Function that checks message from server and depending of its value
 * we can exit the code if it conatins value that represents err with parity or with 
 * coordinate or just move on with the game.
 * @param msg 8 bit integer value that contains recived data from server
 */
static void check_msg(uint8_t msg){
	if (msg == 7)
	{
		(void)fprintf(stdout, "./%s All ships are destroyed. You won the game!\n", prog_name);
		exit(0);
	}
	else if (msg == 4)
	{
		(void)fprintf(stdout, "./%s Maximal number of rounds reached. Game lost!\n", prog_name);
		exit(0);
	}
	else if (msg == 8)
	{
		(void)fprintf(stderr, "./%s ERROR: parity error \n", prog_name);
		exit(2);
	}
	else if (msg == 12)
	{
		(void)fprintf(stderr, "./%s ERROR: invalid coordinate \n", prog_name);
		exit(3);
	}
	else
		return;
}

/**
* @brief Program entry point. Where the whole socket implemenation fro client can be found
* @param argc The argument counter.
* @param argv The argument vector
* @details global variables: prog_name, port, ai
* @return EXIT_FAILURE on failure or EXIT_SUCCESS on success.
*/
int main(int argc, char *argv[])
{

	parse_args(argc, argv);
	uint8_t recv_data;
	uint8_t send_data;
	char buffer[1];

	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	int res = getaddrinfo(host, port, &hints, &ai);
	if (res != 0){
		(void)fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));
		exit(EXIT_FAILURE);
	}

	sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
	if (sockfd < 0){
		(void)fprintf(stderr, "Socket creation failed");
		exit(EXIT_FAILURE);
	}

	if (connect(sockfd, ai->ai_addr, ai->ai_addrlen) < 0){
		(void)fprintf(stderr, "Connection creation failed");
		exit(EXIT_FAILURE);
	}

	srand(time(NULL));
	int round_num = 1;
	send_data = 1;
	//int temp = send_data;
	//send_data = hang_parity(send_data);
	while (round_num <= 80)
	{
		/* Send message to the server */
		send_data = random_try();

		printf("./%s Round: %d \n",prog_name, round_num);
		printf("./%s send_data: %d \n",prog_name, send_data);
		send_data = hang_parity(send_data);
		printf("./%s send_data with parity: %d \n",prog_name, send_data);

		buffer[0] = send_data;
		int n;
		n = write(sockfd, buffer, 1);

		if (n < 0)
		{
			perror("ERROR writing to socket");
			exit(1);
		}

		/* Now read server response */
		n = read(sockfd, buffer, 1);

		if (n < 0)
		{
			perror("ERROR reading from socket");
			exit(1);
		}
		recv_data = buffer[0];
		printf("./%s recieved data: %d\n",prog_name, recv_data);
		check_msg(recv_data);
		round_num++;
	//	temp += 1;
	//	send_data = hang_parity(temp);
	}

	// clean everything
	freeaddrinfo(ai);
	close(sockfd);
	return 0;
}
