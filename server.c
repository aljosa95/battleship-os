/**
 * @file server.c
 * @author Alija-Ali Abadzic 01427575 <e01427575@student.tuwien.ac.at>
 * @date Noevember 2017
 *
 * @brief Server for OSUE exercise 1B `Battleship'.
 * In this server file is server socket implementation and also
 * other background implementation of game battleship.
 */

// IO, C standard library, POSIX API, data types:
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>

// Assertions, errors, signals:
#include <assert.h>
#include <errno.h>
#include <signal.h>

// Time:
#include <time.h>

// Sockets, TCP, ... :
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

// stuff shared by client and server:
#include "common.h"
// number of ships in game
#define SHIP_NUM 6

// Static variables for things you might want to access from several functions:
static const char *port = DEFAULT_PORT; // the port to bind to

// Static variables for resources that should be freed before exiting:
static struct addrinfo *ai = NULL; // addrinfo struct
static int sockfd = -1;			   // socket file descriptor
static int connfd = -1;			   // connection file descriptor


 // Static variable for storing programme name
static char *prog_name = "server";

// in this struct 'coords' we save infos(each coordinate of ship and is this coordinate been hited) about each of our ship
struct coords
{
	bool hit_flag;
	int coord;
};

/**
 * array of struct 'ship' where we save all other infos of each ship (like length, actual size, position)
 * and this struct also conatins other struct 'coords' where we store each coordinate of ship
 **/
struct ship
{
	int length;
	int act_size;
	int first_coord;
	int last_coord;
	bool vertical;
	struct coords ship_coords[4];
} ships[6];

// Static variable that gives us actual number of still existing ships
static int actual_ships = 6;
// 2d array for printing map of hits that user sends
uint8_t map[MAP_SIZE][MAP_SIZE];

/**
 * Mandatory usage function.
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: prog_name
 */
static void usage(void){
	(void)fprintf(stderr, "Usage:./%s [-p PORT] SHIP1...\n", prog_name);
	exit(EXIT_FAILURE);
}

/**
 * @brief This function check right at the beginning of our programm are values 
 * that we pass to server over command line in correct form 
 * (1. und 3. character has to bo letter, 2. and 4. digit, than ships need to be horizontal or vertical set)
 * @details global variables: prog_name
 * @param coordinates Contains first and last coordinate for every ship that has been called
 * @return bool value that indicate are coordinates correct
 */
static bool checkCoordinates(char *coordinates){

	int cord_size = strlen(coordinates);
	if (cord_size < 4 || cord_size > 4)
	{
		(void)fprintf(stderr, "./%s ERROR: wrong syntax for ship coordinates: %s\n", prog_name, coordinates);
		exit(EXIT_FAILURE);
		return false;
	}

	if (!isalpha(coordinates[0]) || !isalpha(coordinates[2]))
	{
		(void)fprintf(stderr, "./%s ERROR: wrong syntax for ship coordinates: %s\n", prog_name, coordinates);
		exit(EXIT_FAILURE);
		return false;
	}

	if (!isdigit(coordinates[1]) || !isdigit(coordinates[3]))
	{
		(void)fprintf(stderr, "./%s ERROR: wrong syntax for ship coordinates: %s\n", prog_name, coordinates);
		exit(EXIT_FAILURE);
		return false;
	}

	int i = 0;
	while (coordinates[i] != 0)
	{
		coordinates[i] = toupper(coordinates[i]);
		i++;
	}

	// coordinates can not be out of range
	int a = 65;
	int j = 74;
	if (coordinates[0] < a || coordinates[0] > j || coordinates[2] < a || coordinates[2] > j)
	{
		(void)fprintf(stderr, "%s ERROR: coordinates outside of map: %s\n", prog_name, coordinates);
		exit(EXIT_FAILURE);
		return false;
	}

	if ((coordinates[1] != coordinates[3]) && (coordinates[0] != coordinates[2]))
	{
		(void)fprintf(stderr, "%s ERROR: ships must be aligned either horizontally or vertically: %s\n", prog_name, coordinates);
		exit(EXIT_FAILURE);
		return false;
	}

	return true;
}

/**
 * @brief This function is being called as we go through all of ships that we give in command line 
 * and store all of their properties(lenght, first and last coordinates as well as all coordinates between those two)
 *  in strucht ships.
 * @details global variables: prog_name, ships
 * @param coords Coordinate that we write in command line in form like b2b5
 * @param k Is index that help us to decide in which ship we actually write in the moment
 */
static void add_ship(char *coords, int k){
	int x;
	for (int i = 0; i < strlen(coords); i++)
	{
		if (i == 0 || i == 2)
		{
			char letter = coords[i];
			x = letter - 65;
		}
		else
		{
			int y = coords[i] - 48; // cause we got hier char num like '53' so minus '48' which is 0 in ascii
			if (i == 1)
			{
				ships[k].first_coord = x + y * 10;
			}
			else
			{
				ships[k].last_coord = x + y * 10;
			}
		}
	}
	// do swap to get a natural order of coords
	if (ships[k].first_coord > ships[k].last_coord)
	{
		int last = ships[k].last_coord;
		ships[k].last_coord = ships[k].first_coord;
		ships[k].first_coord = last;
	}
	// check if ship is vertical or horiznal placed
	if (coords[0] == coords[2])
	{
		ships[k].vertical = true;
	}
	else
	{
		ships[k].vertical = false;
	}
	//decide ship length
	if (ships[k].vertical == true)
	{
		ships[k].length = (ships[k].last_coord - ships[k].first_coord) / 10 + 1;
		ships[k].act_size = ships[k].length;
	}
	else
	{
		ships[k].length = ships[k].last_coord - ships[k].first_coord + 1;
		ships[k].act_size = ships[k].length;
	}
	// check if ships length is too big
	if (ships[k].length > 4 || ships[k].length < 2)
	{
		(void)fprintf(stderr, "./%s ERROR: wrong syntax for ship coordinates: %s\n", prog_name, coords);
		exit(EXIT_FAILURE);
	}
	for (int m = 0; m < 4; m++)
	{
		ships[k].ship_coords[m].coord = -1;
	}
	int temp_c = ships[k].first_coord;
	// adds all coordinates between first and last including them as well of each ship 
	if (ships[k].vertical == true)
	{
		int j;
		for (j = 0; j < ships[k].length; j++)
		{
			ships[k].ship_coords[j].coord = temp_c;
			ships[k].ship_coords[j].hit_flag = false;
			temp_c += 10;
		}
	}
	else
	{
		for (int j = 0; j < ships[k].length; j++)
		{
			ships[k].ship_coords[j].coord = temp_c;
			ships[k].ship_coords[j].hit_flag = false;
			temp_c += 1;
		}
	}
}

/**
 * @brief Function that parses all arguments that we write as we start the programme.
 * It also contains one for loop where we call add_ship function for each boat.
 * @details global variables: prog_name, ships, port
 * @param argc The argument counter.
 * @param argv The argument vector
 */
static void parse_args(int argc, char **argv){
	int c;
	if (argc < 0 || argc > 9)
	{
		usage();
	}

	while ((c = getopt(argc, argv, "p:")) != -1)
	{
		switch (c)
		{
		case 'p':
			(void)fprintf(stdout, "./%s new port: %s  \n",prog_name, optarg);
			fflush(stdout);
			port = optarg;
			break;

		case '?':
			usage();
			break;
		default:
			assert(0);
			usage();
			break;
		}
	}

	if (argc != 7 && argc != 9)
	{
		usage();
	}

	int num = 1;
	int i;
	// if we have also new port, than we skip those args
	if (argc > 7)
		num += 2;
	int k = 0;
	for (i = num; i < argc; i++)
	{
		// we check if our input with coordinates correct is
		bool cord_correct = checkCoordinates(argv[i]);
		if (cord_correct)
		{
			add_ship(argv[i], k);
		}
		k++;
	}
}

/**
 * @brief Function that parses all arguments that we write as we start the programme.
 * It also contains one for loop where we call add_ship function for each boat.
 * @details global variables: prog_name, ships
 * @param argc The argument counter.
 * @param argv The argument vector
 */
static bool check_parity(uint8_t recv_data){
	uint8_t count_ones;
	int k = 1;
	for (int i = 0; i < 8; i++)
	{
		if (recv_data & (k << i))
		{
			count_ones++;
		}
	}
	if (count_ones % 2 == 0)
	{
		return true;
	}
	return false;
}

/**
 * @brief Function that removes parity bit from the message that
 * server gets from client and this represents coordinate in a field.
 * @param recv_data 8 bit integer value that contains coordinate info with parity
 * @return message without parity at the begin of 8 bit value
 */
static uint8_t remove_parity(uint8_t recv_data){
	return recv_data &= 0x7f;
}

/**
 * @brief Function that tell us is a field that client sends outside of our map, 
 * means bigger then the whole game field.
 * @param recv_data 8 bit integer value that contains coordinate info
 * @return boolean value that is true wenn coordinate are in good range
 */
static bool recv_coord(uint8_t recv_data){
	if (recv_data < 100)
	{
		if (recv_data >= 0)
		{
			return true;
		}
	}
	return false;
}

/**
 * @brief Function that in first set a hit or miss depending client's message which has been sent
 * in a map of our game and as second returns appropriate value to client back, 
 * and this return also depends is ship just been hit or whole destoryed or whole missed.
 * And set coordinate flag to hit if it is really been shotted.
 * @details global variables: ships, acutal_ships
 * @param recv_data 8 bit integer value that conatines coordinate info
 * @return value 0,1,2 or 3 depending on was it miss or hit
 */
static int check_guess(uint8_t recv_coord){
	int x = recv_coord / 10;
	int y = recv_coord % 10;
	if (map[y][x] == SQUARE_HIT || map[y][x] == SQUARE_EMPTY)
	{
		return 0;
	}
	for (int i = 0; i < SHIP_NUM; i++)
	{
		int first_c = ships[i].first_coord;
		int last_c = ships[i].last_coord;
		int length = ships[i].length;
		if (first_c <= recv_coord)
		{
			if (last_c >= recv_coord)
			{
				for (int j = 0; j < length; j++)
				{
					if (recv_coord == ships[i].ship_coords[j].coord)
					{
						ships[i].ship_coords[j].hit_flag = true;
						ships[i].act_size--;
						if (ships[i].act_size == 0)
						{
							actual_ships--;
							if (actual_ships == 0)
							{
								map[y][x] = SQUARE_HIT;
								return 3;
							}
							map[y][x] = SQUARE_HIT;
							return 2;
						}
						map[y][x] = SQUARE_HIT;
						return 1;
					}
				}
			}
		}
	}
	map[y][x] = SQUARE_EMPTY;
	return 0;
}

/**
 * @brief Function checks if the number of ships and 
 * their size overlapp with desiered size
 * @details global variables: ships
 * @param recv_data 8 bit integer value that conatines coordinate info
 */
static void ship_order(void){
	int ship2 = 0;
	int ship3 = 0;
	int ship4 = 0;
	for (int i = 0; i < SHIP_NUM; i++)
	{
		if (ships[i].length == 2)
		{
			ship2++;
		}
		if (ships[i].length == 3)
		{
			ship3++;
		}
		if (ships[i].length == 4)
		{
			ship4++;
		}
	}
	if (ship4 != SHIP_CNT_LEN4)
	{
		usage();
	}
	if (ship3 != SHIP_CNT_LEN3)
	{
		usage();
	}
	if (ship2 != SHIP_CNT_LEN2)
	{
		usage();
	}
}

/**
* @brief Program entry point. Where the whole socket implemenation can be found
* @param argc The argument counter.
* @param argv The argument vector
* @details global variables: ships, prog_name, port, ai
* @return EXIT_FAILURE on failure or EXIT_SUCCESS on success.
*/
int main(int argc, char *argv[]){
	
	parse_args(argc, argv);
	ship_order();
	
	/**
	 * Just for testing purposes, prints all infos for each ship
	 */
	for (int i = 0; i < SHIP_NUM; i++){
		(void)fprintf(stdout, "%d.ship first coord: %d  last coord: %d vertical: %d  act_size: %d	", i + 1, ships[i].first_coord, ships[i].last_coord, ships[i].vertical, ships[i].act_size);
		fflush(stdout);
		printf("coords: ");
		for (int j = 0; j < ships[i].length; j++)
		{
			printf("%d ", ships[i].ship_coords[j].coord);
		}
		printf("\n");
	}

	(void)fprintf(stdout, "./%s PORT: %s  \n",prog_name,port);
	fflush(stdout);

	memset(&map, 0, sizeof(map)); // initialize each square as unknown
	print_map(map);

	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	char buffer[8];

	int res = getaddrinfo(NULL, port, &hints, &ai);
	if (res != 0){
		(void)fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));
		exit(EXIT_FAILURE);
	}

	sockfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
	if (sockfd < 0){
		(void)fprintf(stderr, "Socket creation failed\n");
		exit(EXIT_FAILURE);
	}

	int val = 1;
	res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof val);
	if (res < 0){
		(void)fprintf(stderr, "Setsockopt creation failed\n");
		exit(EXIT_FAILURE);
	}

	res = bind(sockfd, ai->ai_addr, ai->ai_addrlen);
	if (res < 0){
		(void)fprintf(stderr, "Bind creation failed\n");
		exit(EXIT_FAILURE);
	}

	res = listen(sockfd, 1);
	if (res < 0){
		(void)fprintf(stderr, "Listen creation failed\n");
		exit(EXIT_FAILURE);
	}

	connfd = accept(sockfd, NULL, NULL);
	if (connfd < 0){
		(void)fprintf(stderr, "Accept creation failed\n");
		exit(EXIT_FAILURE);
	}

	int n;
	// variable that store acutal round number
	int round_num = 1;

	while (round_num <= 80){
		/* Read a message from the client */
		n = read(connfd, buffer, 1);
		if (n < 0)
		{
			perror("ERROR reading from socket");
			exit(1);
		}
		printf("/**** New round ****/\n./%s Round: %d \n",prog_name, round_num);
		uint8_t recv_data = buffer[0];
		uint8_t send_data;
		printf("./%s Recieved data with parity: %d\n",prog_name, recv_data);
		bool correct_parity = check_parity(recv_data);
		if (correct_parity != true){
			send_data = 1 << 3;
			buffer[0] = send_data;
			n = send(connfd, buffer, sizeof(buffer), 0);

			if (n < 0)
			{
				perror("ERROR writing to socket");
				exit(1);
			}
			(void)fprintf(stderr, "./%s ERROR: parity error \n", prog_name);
			exit(2);
			close(sockfd);
			close(connfd);
		}
		recv_data = remove_parity(recv_data);
		printf("./%s Recieved data without parity: %d\n",prog_name, recv_data);

		bool check_coord = recv_coord(recv_data);
		if (check_coord != true){
			send_data = 3 << 2;
			buffer[0] = send_data;
			n = send(connfd, buffer, sizeof(buffer), 0);

			if (n < 0)
			{
				perror("ERROR writing to socket");
				exit(1);
			}
			(void)fprintf(stderr, "./%s ERROR: invalid coordinate \n", prog_name);
			exit(3);
			close(sockfd);
			close(connfd);
		}

		int hit = check_guess(recv_data);
		int status = 0;

		if (hit == 3)
		{
			status = 1 << 2;
		}
		send_data = hit | status;
		if(round_num == 80 && hit!=3){
			send_data = 4;
		}
		printf("./%s Here is the message to send: %d\n",prog_name, send_data);
		buffer[0] = send_data;
		/* Write a response to the client */
		n = write(connfd, buffer, 1);
		if (n < 0)
		{
			perror("ERROR writing to socket");
			exit(1);
		}

		print_map(map);
		printf("./%s %d ship(s) remaining \n",prog_name, actual_ships);
		if (send_data == 7)
		{
			(void)fprintf(stdout, "./%s Client wins in %d rounds\n", prog_name, round_num);
			exit(0);
		}
		// 80 rounds and game lost
		if (send_data == 4)
		{
			(void)fprintf(stdout, "./%s Maximal number of rounds reached. Game lost!\n", prog_name);
			exit(0);
		}
		round_num++;
	}

	//clean everything
	freeaddrinfo(ai);
	close(sockfd);
	close(connfd);
	return 0;
	exit(0);
}
