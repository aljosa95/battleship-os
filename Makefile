###
#
# Author : Alija-Ali Abadzic (01427575) <e01427575@student.tuwien.ac.at>
# Date : 18-10-2017
# Program server.c client.c
#
###

CFLAGS = -std=c99 -pedantic -Wall -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_SVID_SOURCE -D_POSIX_C_SOURCE=200809L  -g -c

all: server client

server: server.o
	gcc server.o -o  server
 
server.o:
	gcc $(CFLAGS) server.c

client: client.o
	gcc client.o -o  client
 
client.o:
	gcc $(CFLAGS) client.c

clean: 
	rm -f *.o
	rm -f server
	rm -f client

fresh: clean server client


